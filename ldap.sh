#!/usr/bin/env bash
set -e
CONTAINER_NAME="gourd-ldap-tester"
ADMIN_PASSWORD="s3cret"

docker stop $CONTAINER_NAME 2> /dev/null || true
docker run --name $CONTAINER_NAME --detach --rm -p 127.0.0.1:389:389 --env LDAP_ORGANISATION="Fricloud" --env LDAP_DOMAIN="fricloud.dk" --env LDAP_ADMIN_PASSWORD="$ADMIN_PASSWORD" osixia/openldap:1.4.0

docker cp ./ldifs/usergroup.ldif $CONTAINER_NAME:/tmp/
docker cp ./ldifs/testuser.ldif $CONTAINER_NAME:/tmp/

sleep 10
docker exec $CONTAINER_NAME ldapadd -Y EXTERNAL -H ldapi:/// -f /tmp/usergroup.ldif
docker exec $CONTAINER_NAME ldapadd -Y EXTERNAL -H ldapi:/// -f /tmp/testuser.ldif
