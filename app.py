from flask import Flask, request, session, redirect, render_template
import os
import yaml
import ldap
import sqlite3
import uuid
from datetime import timedelta

app = Flask(__name__)
myConfig = None

def getDB():
    return sqlite3.connect('database.db')

dbCon = getDB()
dbCon.execute('CREATE TABLE IF NOT EXISTS invites (code TEXT UNIQUE, created_by TEXT, used_by TEXT)')
try:
    dbCon.execute('INSERT INTO invites (code, created_by, used_by) VALUES ("hello", "hello", "")')
    dbCon.commit()
except sqlite3.IntegrityError:
    pass
dbCon.close()


# TODO: Cookies expire after 5 minutes, but not connections
# In general, figure out if this is a good idea
conMap = {}

def logged_in():
    return "username" in session

def usernameToDN(username):
    return "cn=" + username + "," + myConfig["base"]

@app.route('/login/', methods=['POST', 'GET'])
def login_form():
    if logged_in():
        return redirect("/")
    if request.method == 'POST':
        if "username" not in request.form or "password" not in request.form:
            return "You must write a username and password"
        con = ldap.initialize(myConfig["ldap_uri"])
        try:
            con.simple_bind_s(usernameToDN(request.form['username']), request.form['password'])
        except ldap.INVALID_CREDENTIALS:
            return "Wrong username or password"
        session.permament = True
        session['username'] = request.form['username']
        conMap[request.form['username']] = con
        return redirect("/")
    else:
        return render_template('login.html')

@app.route('/password/', methods=['POST', 'GET'])
def password_form():
    if not logged_in():
        return redirect("/login/")
    if request.method == "GET":
        return render_template('password.html')
    if request.method == "POST":
        con = conMap[session['username']]
        if 'password' not in request.form or 'password2' not in request.form or 'oldPassword' not in request.form:
            return "All fields must be filled"
        if request.form['password'] != request.form['password2']:
            return "The two passwords must match"
        newPassword = request.form['password']
        # TODO: password requirement, make user type old pass
        con.passwd_s(usernameToDN(session['username']), request.form['oldPassword'], newPassword)
        return "Password changed"

@app.route('/invite/<code>/')
def useInvite(code):
    dbCon = getDB()
    cur = dbCon.cursor()
    print(code)
    print(type(code))
    cur.execute("SELECT * FROM invites WHERE code=?", (code,))
    rows = cur.fetchall();
    dbCon.close()
    print(rows)
    if len(rows) == 0 or rows[0][2] != "":
        return "Invalid invite code"
    return render_template("invite.html", code=code)

@app.route('/invite/list/')
def listInvites():
    if not logged_in():
        return redirect("/")
    dbCon = getDB()
    cur = dbCon.cursor()
    cur.execute("SELECT * FROM invites WHERE created_by=?", (session['username'],))
    rows = cur.fetchall()
    dbCon.close()
    return render_template("listinvites.html", invites=rows)


@app.route('/invite/generate/')
def generateInvite():
    if not logged_in():
        return redirect("/")
    code = str(uuid.uuid4())
    dbCon = getDB()
    cur = dbCon.cursor()
    cur.execute("SELECT * FROM invites WHERE created_by=?", (session['username'],))
    rows = cur.fetchall()
    if len(rows) > 10:
        dbCon.close()
        return "Each user is only allowed to generate 10 invites"
    dbCon.execute('INSERT INTO invites (code, created_by, used_by) VALUES (?, ?, "")', (code, session['username']))
    dbCon.commit()
    url = myConfig['base_uri'] + '/invite/' + code
    return "Send your friend this link: <a href='" + url + "'>" + code + "</a>"


@app.route('/invite/apply/', methods=['POST'])
def applyInvite():
    if 'username' not in request.form or 'password' not in request.form or 'password2' not in request.form or 'code' not in request.form:
        print(request.form)
        return "All fields must be filled"
    if request.form['password'] != request.form['password2']:
        return "Passwords did not match"
    code = request.form['code']
    username = request.form['username']
    password = request.form['password']
    dbCon = getDB()
    cur = dbCon.cursor()
    cur.execute("SELECT * FROM invites WHERE code=?", (code,))
    rows = cur.fetchall();
    if not len(rows) > 0:
        return "Invalid invite code"
    if rows[0][2] != "":
        return "This code has already been used"
    ldap_con = ldap.initialize(myConfig["ldap_uri"])
    ldap_con.simple_bind_s(myConfig["bind_dn"], myConfig["bind_password"])
    try:
        ldap_con.add_s(usernameToDN(username), [("uid", str.encode(username)), ("objectClass", [b"inetOrgPerson", b"person"]), ("sn", str.encode(username)), ("cn", str.encode(username))])
    except ldap.ALREADY_EXISTS:
        return "This username has already been taken"
    ldap_con.passwd_s(usernameToDN(username), None, password)
    dbCon.execute("UPDATE invites SET used_by=? WHERE code=?", (username,code))
    dbCon.commit()
    dbCon.close()

    return "User created"

@app.route('/')
def hello_world():
    if not logged_in():
        return redirect("/login/")
    return render_template("index.html", name=session['username'])

app.secret_key = os.urandom(12)
app.config['PERMANENT_SESSION_LIFETIME'] =  timedelta(minutes=5)
with open("./config.yaml", "r") as configFile:
    myConfig = yaml.load(configFile, Loader=yaml.FullLoader)
