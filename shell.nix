{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  buildInputs = [
    pkgs.python37 pkgs.python37Packages.pyyaml pkgs.python37Packages.ldap pkgs.python37Packages.flask
  ];
}
